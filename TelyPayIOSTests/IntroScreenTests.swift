//
//  IntroScreenTests.swift
//  TelyPayIOSTests
//
//  Created by admin on 1/30/19.
//  Copyright © 2019 admin. All rights reserved.
//

import XCTest
//@testable import TelyPayIOS

class IntroScreenTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

//    func testSkipBtnTitle() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let introScreen = storyboard.instantiateInitialViewController() as! IntroScreenViewController
//        let _ = introScreen.view
//        let skipBtnSlide1 = introScreen.slides[0].skipBtn
//        let skipBtnSlide2 = introScreen.slides[1].skipBtn
//        let skipBtnSlide3 = introScreen.slides[2].skipBtn
//
//        XCTAssertEqual(NSLocalizedString("skip_btn", comment: ""), skipBtnSlide1?.currentTitle!)
//        XCTAssertEqual(NSLocalizedString("skip_btn", comment: ""), skipBtnSlide2?.currentTitle!)
//        XCTAssertEqual(NSLocalizedString("skip_btn", comment: ""), skipBtnSlide3?.currentTitle!)
//    }
//
//    func testNextBtnTitle() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let introScreen = storyboard.instantiateInitialViewController() as! IntroScreenViewController
//        let _ = introScreen.view
//        let nextBtnSlide1 = introScreen.slides[0].nextBtn
//        let nextBtnSlide2 = introScreen.slides[1].nextBtn
//        let nextBtnSlide3 = introScreen.slides[2].nextBtn
//
//        XCTAssertEqual(NSLocalizedString("next_btn", comment: ""), nextBtnSlide1?.currentTitle!)
//        XCTAssertEqual(NSLocalizedString("next_btn", comment: ""), nextBtnSlide2?.currentTitle!)
//        XCTAssertEqual(NSLocalizedString("next_btn", comment: ""), nextBtnSlide3?.currentTitle!)
//    }
//
//    func testSlideTitleLbl() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let introScreen = storyboard.instantiateInitialViewController() as! IntroScreenViewController
//        let _ = introScreen.view
//        let titLblSlide1 = introScreen.slides[0].slideTitLbl
//        let titLblSlide2 = introScreen.slides[1].slideTitLbl
//        let titLblSlide3 = introScreen.slides[2].slideTitLbl
//
//        XCTAssertEqual(NSLocalizedString("intro_screen_1_tv_a", comment: ""), titLblSlide1?.text!)
//        XCTAssertEqual(NSLocalizedString("intro_screen_2_tv_a", comment: ""), titLblSlide2?.text!)
//        XCTAssertEqual(NSLocalizedString("intro_screen_3_tv_a", comment: ""), titLblSlide3?.text!)
//    }
//
//    func testSlideDescLbl() {
//
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let introScreen = storyboard.instantiateInitialViewController() as! IntroScreenViewController
//        let _ = introScreen.view
//        let descLblSlide1 = introScreen.slides[0].slideDescLbl
//        let descLblSlide2 = introScreen.slides[1].slideDescLbl
//        let descLblSlide3 = introScreen.slides[2].slideDescLbl
//
//        XCTAssertEqual(NSLocalizedString("intro_screen_1_tv_b", comment: ""), descLblSlide1?.text!)
//        XCTAssertEqual(NSLocalizedString("intro_screen_2_tv_b", comment: ""), descLblSlide2?.text!)
//        XCTAssertEqual(NSLocalizedString("intro_screen_3_tv_b", comment: ""), descLblSlide3?.text!)
//
//
//
//    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
