//
//  NotificationService.swift
//  TelypayNotificationSerExtintion
//
//  Created by Rabaa Al Falahi on 8/1/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            
            
        
                
                if (bestAttemptContent.body == "IDA"){

                    bestAttemptContent.title = NSLocalizedString("ida_tit", comment: "")
                    bestAttemptContent.body = NSLocalizedString("ida", comment: "")
                    
                } else if (bestAttemptContent.body == "IDR"){

                    bestAttemptContent.title = NSLocalizedString("idr_tit", comment: "")
                    bestAttemptContent.body = NSLocalizedString("idr", comment: "")
                    
                } else if (bestAttemptContent.body == "TUA"){

                    bestAttemptContent.title = NSLocalizedString("tua_tit", comment: "")
                    bestAttemptContent.body = NSLocalizedString("tua", comment: "")
                    
                } else if (bestAttemptContent.body == "TUR"){
                    
                    bestAttemptContent.title = NSLocalizedString("tur_tit", comment: "")
                    bestAttemptContent.body = NSLocalizedString("tur", comment: "")
                    
                } else {
                    
                    //
                }
                
                
            
//            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
//            print("bbbbbb \(String(describing: bestAttemptContent.body))")

            contentHandler(bestAttemptContent)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
