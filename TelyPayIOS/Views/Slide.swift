//
//  Slide.swift
//  TelyPayIOS
//
//  Created by admin on 9/30/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class Slide: UIView {

    @IBOutlet weak var slideImg: UIImageView!
    @IBOutlet weak var slideTitLbl: UILabel!
    @IBOutlet weak var slideDescLbl: UITextView!
     @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    
    @IBOutlet weak var topCostraintCardView: NSLayoutConstraint!
    
    /*
     // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func draw(_ rect: CGRect) {
        
//      cardView.dropShadow()
        nextBtn.dropShadow()
        nextBtn.setTitle(NSLocalizedString("next_btn", comment: ""), for: .normal)
        skipBtn.setTitle(NSLocalizedString("skip_btn", comment: ""), for: .normal)
        skipBtn.dropShadow()
        skipBtn.layer.borderColor = #colorLiteral(red: 0.1006977562, green: 0.2920336034, blue: 0.5024970988, alpha: 1)
        skipBtn.layer.borderWidth = 2
        
        let isIPhoneX = AppDelegate.init().isIPhoneXDevice()
        if isIPhoneX {
            topCostraintCardView.constant = 120
            self.addConstraint(topCostraintCardView)
        }else
        {
            topCostraintCardView.constant = 76
            self.addConstraint(topCostraintCardView)
        }
    }
    

}
