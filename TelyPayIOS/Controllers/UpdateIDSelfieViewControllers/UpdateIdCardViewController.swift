//
//  UpdateIdCardViewController.swift
//  TelyPayIOS
//
//  Created by admin on 6/23/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit
import AVFoundation


class UpdateIdCardViewController: BaseViewController, UIPageViewControllerDelegate,
AVCapturePhotoCaptureDelegate{

    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var takeIDPhotoBtn: UIButton!
    @IBOutlet weak var capturedIDImg: UIImageView!
    @IBOutlet weak var btnsStackView: UIStackView!
    @IBOutlet weak var retakeBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    
    
   
    
    @IBAction func nextBtnAction(_ sender: Any) {
        let parentVC = self.parent as! UpdateIDSelfieViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[2]], direction: .reverse, animated: true, completion: nil)
        SignUpViewController.pageControl.currentPage = 2
        
    }
    
    @IBAction func goBackAction(_ sender: Any) {
        
        let parentVC = self.parent as! UpdateIDSelfieViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[0]], direction: .reverse, animated: true, completion: nil)
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        previewView.sendSubviewToBack(takeIDPhotoBtn)
        capturedIDImg.isHidden = true
        btnsStackView.isHidden = true
        
        takeIDPhotoBtn.layer.borderColor = #colorLiteral(red: 0.1006977562, green: 0.2920336034, blue: 0.5024970988, alpha: 1)
        takeIDPhotoBtn.layer.borderWidth = 1
        
        retakeBtn.setTitle(NSLocalizedString("rtake_pic_btn_txt", comment: ""), for: .normal)
        nextBtn.setTitle(NSLocalizedString("next_btn", comment: ""), for: .normal)
        
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
        guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
            else {
                print("Unable to access back camera!")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: backCamera)
            
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                captureSession.addInput(input)
                captureSession.addOutput(stillImageOutput)
                setupLivePreview()
            }
            
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
        
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspect
        videoPreviewLayer.connection?.videoOrientation = .portrait
        previewView.layer.addSublayer(videoPreviewLayer)
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
            self.captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.previewView.bounds
            }
        }
        
    }
    
    @IBAction func takeIDPhotoAction(_ sender: Any) {
        
        previewView.isHidden = true
        capturedIDImg.isHidden = false
        btnsStackView.isHidden = false
        takeIDPhotoBtn.isHidden = true
        btnsStackView.bringSubviewToFront(view)
        capturedIDImg.sendSubviewToBack(btnsStackView)
        
        if #available(iOS 11.0, *) {
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
            
        } else {
            // Fallback on earlier versions
            let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG])
            stillImageOutput.capturePhoto(with: settings, delegate: self)
        }
        
    }
    
    
    @available(iOS 11.0, *)
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)
        capturedIDImg.image = image
        let imgStr = TakeIDCardPhotoViewController.ConvertImageToBase64String(img: image!)
        //print("id card image string \(imgStr)")
        let parentVC = self.parent as! UpdateIDSelfieViewController
        parentVC.imgStr = imgStr
        
    }
    
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let sampleBuffer = photoSampleBuffer,
            let previewBuffer = previewPhotoSampleBuffer,
            let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let image = UIImage(data: dataImage)
            capturedIDImg.image = image
            let imgStr = TakeIDCardPhotoViewController.ConvertImageToBase64String(img: image!)
            //print("id card image string \(imgStr)")
            let parentVC = self.parent as! UpdateIDSelfieViewController
            parentVC.imgStr = imgStr
            
        }
        
    }
    
    
    @IBAction func retakeIDPhotoBtnAction(_ sender: Any) {
        previewView.isHidden = false
        capturedIDImg.isHidden = true
        btnsStackView.isHidden = true
        takeIDPhotoBtn.isHidden = false
        
    }
    
    //this function to covert the image to string
    public static func ConvertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = img.jpegData(compressionQuality: 0.50)! as NSData //UIImagePNGRepresentation(img)
        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
