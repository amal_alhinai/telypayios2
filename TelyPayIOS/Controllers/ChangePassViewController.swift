//
//  ChangePassViewController.swift
//  TelyPayIOS
//
//  Created by admin on 11/4/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView

class ChangePassViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var oldPassTxt: RSFloatInputView!
    @IBOutlet weak var newPassTxt: RSFloatInputView!
    @IBOutlet weak var confirmNewPassTxt: RSFloatInputView!
    
    @IBOutlet weak var oldPassErrLbl: UILabel!
    @IBOutlet weak var newPassErrLbl: UILabel!
    @IBOutlet weak var confirmPassErrLbl: UILabel!
    
    @IBOutlet weak var changePassBtn: UIButton!
    
    @IBOutlet weak var pageTitle: UINavigationItem!
    
    //let keyChainWrapper = KeyChainWrapperManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        oldPassTxt.textField.delegate = self
        newPassTxt.textField.delegate = self
        confirmNewPassTxt.textField.delegate = self
        oldPassTxt.textField.returnKeyType = .done
        newPassTxt.textField.returnKeyType = .done
        confirmNewPassTxt.textField.returnKeyType = .done
        

        pageTitle.title = NSLocalizedString("change_password_txt", comment: "")
        oldPassTxt.placeHolderLabel.string = NSLocalizedString("old_pass_ed_hint", comment: "")
        oldPassTxt.layer.borderColor = UIColor.white.cgColor
        oldPassTxt.textField.textColor = UIColor.white
        newPassTxt.layer.borderColor = UIColor.white.cgColor
        newPassTxt.placeHolderLabel.string = NSLocalizedString("new_pass_ed_hint", comment: "")
        newPassTxt.textField.textColor = UIColor.white
        confirmNewPassTxt.placeHolderLabel.string = NSLocalizedString("confirm_new_pass_ed_hint", comment: "")
        confirmNewPassTxt.layer.borderColor = UIColor.white.cgColor
        confirmNewPassTxt.textField.textColor = UIColor.white
        changePassBtn.setTitle(NSLocalizedString("change_password_txt", comment: ""), for: .normal)
        
       /// when type remove the error label
        oldPassTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        newPassTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                          for: UIControl.Event.editingChanged)
        
        confirmNewPassTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                        for: UIControl.Event.editingChanged)
        
        // Do any additional setup after loading the view.
    }

    @IBAction func changePassBtnAction(_ sender: Any) {
        
        
        if (oldPassTxt.textField.text?.isEmpty)! || (newPassTxt.textField.text?.isEmpty)!
            {
            
            if (oldPassTxt.textField.text?.isEmpty)!{
                oldPassErrLbl.isHidden = false
                oldPassErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            if (newPassTxt.textField.text?.isEmpty)!{
                newPassErrLbl.isHidden = false
                newPassErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            if (confirmNewPassTxt.textField.text?.isEmpty)!{
                confirmPassErrLbl.isHidden = false
                confirmPassErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
        }else {
        
            if validation() {
        changePassRequest()
            }
        }
    }
    
    /// this func for checking if password is valid, new pass and confirm new pass is same
    func validation() -> Bool {
        
        var noError = true
       
       
        if (!(newPassTxt.textField.text?.isValidPass())!) {
            noError = false
            newPassErrLbl.isHidden = false
            newPassErrLbl.text = NSLocalizedString("invalid_pass_msg", comment: "")
        }
        
        if(newPassTxt.textField.text! != confirmNewPassTxt.textField.text!) {
            noError = false
            confirmPassErrLbl.isHidden = false
            confirmPassErrLbl.text = NSLocalizedString("confirm_new_pass_not_match_err_msg", comment: "")
        }
        
        
        return noError
    }
    
    
    func changePassRequest() {
        
        let parameters = ["Email": keyChainWrapper.getLoggedUserDetails().emailStr,
                          "OldPassword": oldPassTxt.textField.text! ,
                          "NewPassword": newPassTxt.textField.text!]
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!]
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.strRequestPOSTURL(URLs.URL_CHANGE_PASSWORD, params: (parameters as! [String : String]), headers: headers, success: { (response) in
            progressHUD.hide()
            //print(response)
            
            //print(" response \(response["message"])")
            
            if (response["Message"].stringValue == "Password has been changed"){
                
                let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                    
                    self.keyChainWrapper.logout()
                    
                    let loginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? ViewController
                    self.present(loginPage!, animated: true, completion: nil)
                    
                }))
                self.present(alert, animated: true, completion: nil)
                
            }else {
            
                let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            }
            
        }) { (error) in
            progressHUD.hide()
            //print("errrrr \(error)")
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //will be called when the user touches any where in the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide the keyboard from username field
        
        guard let touch:UITouch = touches.first else
        {
            return;
        }
        if touch.view != touch
        {
            oldPassTxt.endEditing(true)
            newPassTxt.endEditing(true)
            confirmNewPassTxt.endEditing(true)

        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == oldPassTxt.textField {
            oldPassErrLbl.isHidden = true
        }
        
        if textField == newPassTxt.textField {
            newPassErrLbl.isHidden = true
        }
        
        if textField == confirmNewPassTxt.textField {
            confirmPassErrLbl.isHidden = true
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
