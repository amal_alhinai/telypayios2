//
//  TopUpViewController.swift
//  TelyPayIOS
//
//  Created by admin on 10/30/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class TopUpViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var voucherNumberTxt: UITextField!
    @IBOutlet weak var topUpBtn: UIButton!
    public static var qrCodeResultStr = ""
    var telecomListArr = [JSON]()
    var selectedTelecom : String?

    //let userDefaul = UserDefaultManager()
    //let keyChainWrapper = KeyChainWrapperManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        voucherNumberTxt.delegate = self
        topUpBtn.setImage( #imageLiteral(resourceName: "topup_btn_inactive"), for: .disabled)
        topUpBtn.isEnabled = false
        
//        getTelecoms()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        getTelecoms()
        
        if !TopUpViewController.qrCodeResultStr.isEmpty {
        voucherNumberTxt.text = TopUpViewController.qrCodeResultStr
            TopUpViewController.qrCodeResultStr = ""
        }
        
        
    }
    

    @IBAction func buttonsAction(_ sender: UIButton) {
        
            checkMaxLength()
            if (voucherNumberTxt.text?.count)! < 14 {
                //topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_inactive")
                
                    if sender.tag == 1 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "0"
                    }
                    else if sender.tag == 100 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "1"
                    }
                    else if sender.tag == 200 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "2"
                    }
                    else if sender.tag == 300 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "3"
                    }
                    else if sender.tag == 400 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "4"
                    }
                    else if sender.tag == 500 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "5"
                    }
                    else if sender.tag == 600 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "6"
                    }
                    else if sender.tag == 700 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "7"
                    }
                    else if sender.tag == 800 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "8"
                    }
                    else if sender.tag == 900 {
                        voucherNumberTxt.text = voucherNumberTxt.text! + "9"
                    }
                
            }
//            else {
//            topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_active")
//
//        }
        
        
        if sender.tag == 1111 {
            voucherNumberTxt.text = String((voucherNumberTxt.text?.dropLast())!)
            checkMaxLength1()
        }
        
        if sender.tag == 9999 {
            self.dismiss(animated: true, completion: nil)
            TopUpViewController.qrCodeResultStr = ""
        }
        
        if sender.tag == 1234 {
            topUpAction()
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func topUpAction() {
       // print(voucherNumberTxt.text?.count)
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let parameters = ["CardNumber": voucherNumberTxt.text!,
                          "CompanyID":"72643469"]
        let headers = ["Content-Type":"application/json; charset=utf-8",
                       "Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestPOSTURL(URLs.URL_TEMP_TOPUP, params: parameters as [String : AnyObject], headers: headers, success: { (response) in
            
            progressHUD.hide()
//            print(response)
     
//            if response["Message"].stringValue.contains("successfully") {
//
//                //this code for getting the part of the message without the ammount for translation  /// 5.000 OMR is added to your wallet successfully.
//                let index = response["Message"].stringValue.index(of: " ")!
//                _ = response["Message"].stringValue.suffix(from: index) // OMR is added to your wallet successfully.
//
//                //this for getting the amount added only
////                let responseMsgArr = response["Message"].stringValue.split(separator: " ")
////                let amountAddedStr = responseMsgArr[0] // for example get this -> 5.000
//
//                self.voucherNumberTxt.text = ""
//
//                let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue) , preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
//
//                    self.dismiss(animated: true, completion: nil)
//                }))
//                self.present(alert, animated: true, completion: nil)
//
////                let alert = UIAlertController(title: "", message: amountAddedStr + " " + AFManager.changeMsg1(MsgCode: response["resCode"].intValue) , preferredStyle: UIAlertController.Style.alert)
////                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
////
////                    self.dismiss(animated: true, completion: nil)
////                }))
////            self.present(alert, animated: true, completion: nil)
//
//            }
//            else {
            
                let alert = UIAlertController(title: "", message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue) , preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
//            }
            
            
        }) { (error) in
            
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func selectTelecomsAlert(arr : [JSON]) {

        
        let alertController : UIAlertController = UIAlertController(title: "Select email ID", message: nil, preferredStyle: .alert)
        alertController.view.backgroundColor = UIColor.white
        alertController.view.layer.cornerRadius = 8.0
        
        let contactNumVC = TelecomTableViewController ()
        
        contactNumVC.numOfRaw = arr.count
        contactNumVC.telecomsArr = arr

        contactNumVC.preferredContentSize = CGSize(width: alertController.view.frame.width, height: (44 * CGFloat(arr.count)))
        
        alertController.setValue(contactNumVC, forKeyPath: "contentViewController")
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
            if self.selectedTelecom == nil {
                self.selectedTelecom = "72643469"

            }else {
                self.selectedTelecom = contactNumVC.selectedTelecom

            }
//            print("ssssss \(String(describing: self.selectedTelecom!))")
        }))
        self.present(alertController, animated: true, completion:nil)

    }
    
    
    func getTelecoms() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Content-Type":"application/json; charset=utf-8",
                       "Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestGETURL(URLs.URL_GET_TELECOMS, headers: headers, success: { (JSONresponse) in
            
            progressHUD.hide()
//            print(JSONresponse)
            
            self.telecomListArr = JSONresponse.arrayValue
//            print("rtrt \(self.telecomListArr[0])")

            self.selectTelecomsAlert(arr: self.telecomListArr)

            
        }) { (error) in
            
            progressHUD.hide()
//            print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }

  
    func checkMaxLength() {
        voucherNumberTxt.reloadInputViews()
        //print(voucherNumberTxt.text?.count)
        if(((voucherNumberTxt.text?.count)! + 1) < 13)
        {
            topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_inactive")
            topUpBtn.isEnabled = false
        }else {
            topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_active")
            topUpBtn.isEnabled = true
        }
        
    }
   
    func checkMaxLength1() {
        voucherNumberTxt.reloadInputViews()
        //print(voucherNumberTxt.text?.count)
        if(((voucherNumberTxt.text?.count)!) < 14)
        {
            topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_inactive")
           topUpBtn.isEnabled = false
        }else {
            topUpBtn.imageView?.image = #imageLiteral(resourceName: "topup_btn_active")
            topUpBtn.isEnabled = true
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
