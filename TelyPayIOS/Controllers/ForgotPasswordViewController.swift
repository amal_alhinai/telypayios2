//
//  ForgotPasswordViewController.swift
//  TelyPayIOS
//
//  Created by admin on 9/26/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView

class ForgotPasswordViewController: BaseViewController, UITextFieldDelegate {
    

    @IBOutlet weak var emailTxt: RSFloatInputView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var enterYourEmailLbl: UILabel!
    @IBOutlet weak var resetPassLinkWillBeSentLbl: UILabel!
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var emailErrLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailTxt.textField.delegate = self
        titleLbl.text = NSLocalizedString("header_forgot_pass", comment: "")
        emailTxt.textField.keyboardType = .emailAddress
        emailTxt.textField.returnKeyType = .done
        emailTxt.dropShadow()
        
        emailTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                     for: UIControl.Event.editingChanged)
        
        emailTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        emailTxt.placeHolderLabel.string = NSLocalizedString("email_ed_hint", comment: "")
        enterYourEmailLbl.text = NSLocalizedString("enter_your_email_text", comment: "")
        resetPassLinkWillBeSentLbl.text = NSLocalizedString("reset_pass_link_willbe_sent_text", comment: "")
        resetBtn.setTitle(NSLocalizedString("reset_btn", comment: ""), for: .normal)
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
        
        // Do any additional setup after loading the view.
    }

    @IBAction func backBtnAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func resetPasswordAction(_ sender: Any) {
        
        if validation() {
            resetPassRequest()
        }
        
        
    }
    
    func resetPassRequest() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let forgotPassUrlStr = URLs.URL_FORGOT_PASSWORD + emailTxt.textField.text!
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestGETURL(forgotPassUrlStr, success: { (response) in
            progressHUD.hide()
            
            //print(response)
            
            let alert = UIAlertController(title: "" , message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }) { (error) in
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func validation() -> Bool {
        
        var noError = true
        
        if (!(emailTxt.textField.text?.trimmingCharacters(in: .whitespaces).isEmail())!) {
            noError = false
            emailErrLbl.isHidden = false
            emailErrLbl.text = NSLocalizedString("invalid_email_msg", comment: "")
        }
        
        if (emailTxt.textField.text?.isEmpty)!{
            emailErrLbl.isHidden = false
            emailErrLbl.text = NSLocalizedString("error_string", comment: "")
        }
        
        return noError
    }
    
    //will be called when the user touches any where in the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide the keyboard from username field
        
        guard let touch:UITouch = touches.first else
        {
            return;
        }
        if touch.view != touch
        {
            emailTxt.endEditing(true)
        }
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == emailTxt.textField {
            emailErrLbl.isHidden = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
