//
//  BaseViewController.swift
//  TelyPayIOS
//
//  Created by admin on 3/7/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit
import Crashlytics

class BaseViewController: UIViewController {

    let keyChainWrapper = KeyChainWrapperManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        //send the username to crashlytics board in firebase console
        Crashlytics.sharedInstance().setUserIdentifier(keyChainWrapper.getLoggedUserDetails().emailStr)
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
