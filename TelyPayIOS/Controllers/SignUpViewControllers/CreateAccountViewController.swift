//
//  CreateAccountViewController.swift
//  TelyPayIOS
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView
import Crashlytics

class CreateAccountViewController: BaseViewController, UIPageViewControllerDelegate, UITextFieldDelegate {

//    var firstNameStr: String = ""
//    var middleNameStr: String = ""
//    var lastNameStr: String = ""
//    var emailStr: String = ""
//    var phoneNumStr: String = ""
//    var civilIDNumStr: String = ""
//    var passwordStr: String = ""
//    var imgStr: String = ""
    

    @IBOutlet weak var firstNameTxt: RSFloatInputView!
    @IBOutlet weak var middleNameTxt: RSFloatInputView!
    @IBOutlet weak var lastNameTxt: RSFloatInputView!
    @IBOutlet weak var emailTxt: RSFloatInputView!
    @IBOutlet weak var civilIdNumberTxt: RSFloatInputView!
    @IBOutlet weak var phoneNumTxt: RSFloatInputView!
    @IBOutlet weak var passwordTxt: RSFloatInputView!
    @IBOutlet weak var helpUsKnowULbl: UILabel!
    @IBOutlet weak var middleNameErrLbl: UILabel!
    @IBOutlet weak var firstNameErrLbl: UILabel!
    @IBOutlet weak var lastNameErrLbl: UILabel!
    @IBOutlet weak var emailErrLbl: UILabel!
    @IBOutlet weak var phoneErrLbl: UILabel!
    @IBOutlet weak var civilIDErrLbl: UILabel!
    @IBOutlet weak var passwordErrLbl: UILabel!
    @IBOutlet weak var captureIDBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    
    @IBAction func nextAction(_ sender: Any) {
        
        if (firstNameTxt.textField.text?.isEmpty)! || (middleNameTxt.textField.text?.isEmpty)! || (lastNameTxt.textField.text?.isEmpty)! || (phoneNumTxt.textField.text?.isEmpty)! || (civilIdNumberTxt.textField.text?.isEmpty)! || (passwordTxt.textField.text?.isEmpty)! || (emailTxt.textField.text?.isEmpty)!   {
            
            if (firstNameTxt.textField.text?.isEmpty)!{
                
                firstNameErrLbl.isHidden = false
                firstNameErrLbl.text = NSLocalizedString("error_string", comment: "")
                
                if !(middleNameTxt.textField.text?.isEmpty)! {
                    middleNameErrLbl.text = " "
                    middleNameErrLbl.isHidden = false
                }
            }
            
            if (middleNameTxt.textField.text?.isEmpty)!{
                
                middleNameErrLbl.isHidden = false
                middleNameErrLbl.text = NSLocalizedString("error_string", comment: "")
                
                if !(firstNameTxt.textField.text?.isEmpty)! {
                    firstNameErrLbl.text = " "
                    firstNameErrLbl.isHidden = false
                }
                
            }
            
            if (lastNameTxt.textField.text?.isEmpty)!{
                lastNameErrLbl.isHidden = false
                lastNameErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            if (emailTxt.textField.text?.isEmpty)!{
                emailErrLbl.isHidden = false
                emailErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            if (phoneNumTxt.textField.text?.isEmpty)!{
                phoneErrLbl.isHidden = false
                phoneErrLbl.text = NSLocalizedString("error_string", comment: "")
                
                if !(civilIdNumberTxt.textField.text?.isEmpty)! {
                    civilIDErrLbl.text = " "
                    civilIDErrLbl.isHidden = false
                }
                
            }
            
            if (civilIdNumberTxt.textField.text?.isEmpty)!{
                civilIDErrLbl.isHidden = false
                civilIDErrLbl.text = NSLocalizedString("error_string", comment: "")
                
                if !(phoneNumTxt.textField.text?.isEmpty)! {
                    phoneErrLbl.text = " "
                    phoneErrLbl.isHidden = false
                }
                
            }
            
            if (passwordTxt.textField.text?.isEmpty)!{
                passwordErrLbl.isHidden = false
                passwordErrLbl.text = NSLocalizedString("error_string", comment: "")
            }
            
            
        }else {
            
            if validation() {
                checkIfEmailExist()
            }
        
        }
    }
    
    
    
    
    /// this func for checking if email, phone, civil id and password have no errors
    func validation() -> Bool {
        
        var noError = true
        
        if (phoneNumTxt.textField.text?.count)! < 8 {
            noError = false
            phoneErrLbl.isHidden = false
            phoneErrLbl.text = NSLocalizedString("invalid_phone_msg", comment: "")
            
            if civilIDErrLbl.isHidden {
                civilIDErrLbl.text = " "
                civilIDErrLbl.isHidden = false
            }
            
        }
        
        if (civilIdNumberTxt.textField.text?.count)! < 8 {
            noError = false
            civilIDErrLbl.isHidden = false
            civilIDErrLbl.text = NSLocalizedString("invalid_civilId_msg", comment: "")
            
            if phoneErrLbl.isHidden {
                phoneErrLbl.text = " "
                phoneErrLbl.isHidden = false
            }
            
        }
        
        if (!(emailTxt.textField.text?.trimmingCharacters(in: .whitespaces).isEmail())!) {
            noError = false
            emailErrLbl.isHidden = false
            emailErrLbl.text = NSLocalizedString("invalid_email_msg", comment: "")
        }
        
        if (!(passwordTxt.textField.text?.isValidPass())!) {
            noError = false
            passwordErrLbl.isHidden = false
            passwordErrLbl.text = NSLocalizedString("invalid_pass_msg", comment: "")
        }
        
        return noError
    }
    
    
    @IBAction func backBtnAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        helpUsKnowULbl.text = NSLocalizedString("help_us_to_know_u_txt", comment: "")
        captureIDBtn.setTitle(NSLocalizedString("capture_id", comment: ""), for: .normal)
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title: "", message: NSLocalizedString("english_only_dialog_msg", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        firstNameTxt.textField.delegate = self
        middleNameTxt.textField.delegate = self
        lastNameTxt.textField.delegate = self
        emailTxt.textField.delegate = self
        phoneNumTxt.textField.delegate = self
        civilIdNumberTxt.textField.delegate = self
        passwordTxt.textField.delegate = self
        
        emailTxt.textField.keyboardType = .emailAddress
        phoneNumTxt.textField.keyboardType = .phonePad
        civilIdNumberTxt.textField.keyboardType = .numberPad
        passwordTxt.textField.isSecureTextEntry = true
        
        firstNameTxt.textField.returnKeyType = .done
        middleNameTxt.textField.returnKeyType = .done
        lastNameTxt.textField.returnKeyType = .done
        emailTxt.textField.returnKeyType = .done
        phoneNumTxt.textField.returnKeyType = .done
        civilIdNumberTxt.textField.returnKeyType = .done
        passwordTxt.textField.returnKeyType = .done
        
        firstNameTxt.dropShadow()
        firstNameTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        firstNameTxt.placeHolderLabel.string = NSLocalizedString("first_name_ed_hint", comment: "")
        middleNameTxt.dropShadow()
        middleNameTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        middleNameTxt.placeHolderLabel.string = NSLocalizedString("middle_name_ed_hint", comment: "")
        lastNameTxt.dropShadow()
        lastNameTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        lastNameTxt.placeHolderLabel.string = NSLocalizedString("last_name_ed_hint", comment: "")
        emailTxt.dropShadow()
        emailTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        emailTxt.placeHolderLabel.string = NSLocalizedString("email_ed_hint", comment: "")
        civilIdNumberTxt.dropShadow()
        civilIdNumberTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        civilIdNumberTxt.placeHolderLabel.string = NSLocalizedString("civilid_ed_hint", comment: "")
        phoneNumTxt.dropShadow()
        phoneNumTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        phoneNumTxt.placeHolderLabel.string = NSLocalizedString("phone_ed_hint", comment: "")
        passwordTxt.dropShadow()
        passwordTxt.layer.borderColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0).cgColor
        passwordTxt.placeHolderLabel.string = NSLocalizedString("password_ed_hint", comment: "")
        
        phoneNumTxt.textField.delegate = self
        civilIdNumberTxt.textField.delegate = self
        
        /// when type remove the error label
        firstNameTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                        for: UIControl.Event.editingChanged)
        
        middleNameTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        lastNameTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        emailTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        
        phoneNumTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                        for: UIControl.Event.editingChanged)
        
        civilIdNumberTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        passwordTxt.textField.addTarget(self, action: #selector(ViewController.textFieldDidChange(_:)),
                                         for: UIControl.Event.editingChanged)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        //print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
       
        
        // Do any additional setup after loading the view.
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField ==  phoneNumTxt.textField) || (textField ==  civilIdNumberTxt.textField) {
            let char = string.cString(using: String.Encoding.utf8)
            let isBackSpace = strcmp(char, "\\b")
            if isBackSpace == -92 {
                return true
            }
            return textField.text!.count <= 7
        }
        
        ///this condition to accept only english in testfields
        let allowedCharacters = CharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz").inverted
        if (textField ==  firstNameTxt.textField) || (textField ==  middleNameTxt.textField) || (textField ==  lastNameTxt.textField) {
        let components = string.components(separatedBy: allowedCharacters)
        let filtered = components.joined(separator: "")
        if string == filtered {
            return true
        } else {
            return false
        }
        }
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
     
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if textField == firstNameTxt.textField || textField == middleNameTxt.textField {
            
            if !middleNameErrLbl.isHidden && firstNameErrLbl.isHidden {
                firstNameErrLbl.isHidden = false
                firstNameErrLbl.text = " "
                
            }
            else if middleNameErrLbl.isHidden && !firstNameErrLbl.isHidden {
                middleNameErrLbl.isHidden = false
                middleNameErrLbl.text = " "
            }
            else {
                firstNameErrLbl.isHidden = true
                middleNameErrLbl.isHidden = true
            }
            
        }

        
        if textField == phoneNumTxt.textField || textField == civilIdNumberTxt.textField {
            
            if !phoneErrLbl.isHidden && civilIDErrLbl.isHidden {
                civilIDErrLbl.isHidden = false
                civilIDErrLbl.text = " "
                
            }
            else if phoneErrLbl.isHidden && !civilIDErrLbl.isHidden {
                phoneErrLbl.isHidden = false
                phoneErrLbl.text = " "
            }
            else {
                phoneErrLbl.isHidden = true
                civilIDErrLbl.isHidden = true
            }
            
        }
        
        if textField == lastNameTxt.textField {
            lastNameErrLbl.isHidden = true
        }
        
        if textField == emailTxt.textField {
            emailErrLbl.isHidden = true
        }
        
//        if textField == phoneNumTxt.textField {
//            phoneErrLbl.isHidden = true
//        }
//
//        if textField == civilIdNumberTxt.textField {
//            civilIDErrLbl.isHidden = true
//        }
        
        if textField == passwordTxt.textField {
            passwordErrLbl.isHidden = true
        }
        
        
        
    }
    
    //this method check the email and civil id if already exist and if its new one go next page
    func checkIfEmailExist() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        let checkEmailURLStr = URLs.URL_CHECK_EMAIL_CIVILID_IF_EXIST + emailTxt.textField.text! + "&Password=" + passwordTxt.textField.text! + "&Phone=" + phoneNumTxt.textField.text! + "&CivilID=" + civilIdNumberTxt.textField.text!;
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestGETURL(checkEmailURLStr, success: { (JSONResponse) in
            progressHUD.hide()
            //print(JSONResponse)
            let msg = JSONResponse["Message"].stringValue
            if(msg == "Doesn't exist"){
                
                
                let parentVC = self.parent as! SignUpViewController
                // change page of PageViewController
                parentVC.setViewControllers([parentVC.pages[1]], direction: .forward, animated: true, completion: nil)
                SignUpViewController.pageControl.currentPage = 1
                
                parentVC.firstNameStr = self.firstNameTxt.textField.text!
                parentVC.middleNameStr = self.middleNameTxt.textField.text!
                parentVC.lastNameStr = self.lastNameTxt.textField.text!
                parentVC.emailStr = self.emailTxt.textField.text!
                parentVC.civilIDNumStr = self.civilIdNumberTxt.textField.text!
                parentVC.phoneNumStr = self.phoneNumTxt.textField.text!
                parentVC.passwordStr = self.passwordTxt.textField.text!
                
            }else{
                
                let alert = UIAlertController(title: "" , message: AFManager.changeMsg1(MsgCode: JSONResponse["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                    //print("alerady exist" + msg)
                
            }
            
            //print(msg)
        }) { (error) in
            progressHUD.hide()
            //print(error)
            
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //will be called when the user touches any where in the view
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //hide the keyboard from username field
        
        guard let touch:UITouch = touches.first else
        {
            return;
        }
        if touch.view != touch
        {
            emailTxt.endEditing(true)
            firstNameTxt.endEditing(true)
            middleNameTxt.endEditing(true)
            phoneNumTxt.endEditing(true)
            lastNameTxt.endEditing(true)
            passwordTxt.endEditing(true)
            civilIdNumberTxt.endEditing(true)
            
            
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        print("keyboardWillShow")
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/2
                //hide title when the keyboard is shown
                SignUpViewController.titleLbl.isHidden = true
                SignUpViewController.pageControl.isHidden = true
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        print("keyboardWillHide")
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0.0
                SignUpViewController.titleLbl.isHidden = false
                SignUpViewController.pageControl.isHidden = false
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


