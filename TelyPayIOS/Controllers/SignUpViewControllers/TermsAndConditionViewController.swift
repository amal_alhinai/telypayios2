//
//  TermsAndConditionViewController.swift
//  TelyPayIOS
//
//  Created by admin on 10/1/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class TermsAndConditionViewController: BaseViewController {

    @IBOutlet weak var termsAndConditionTxtView: UITextView!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // let parentVC = self.parent as! SignUpViewController
        SignUpViewController.pageControl.currentPage = 2
       // parentVC.configureTitleLbl(pageTitle: "last")
        
        termsAndConditionTxtView.text = NSLocalizedString("term_condition", comment: "")
        acceptBtn.setTitle(NSLocalizedString("accept_btn", comment: ""), for: .normal)
        
        /// this code for change the arrow direction of backBtn img
        let locale = NSLocale.current.languageCode
        print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerBtnAction(_ sender: Any) {
    
        onRegistration()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let parentVC = self.parent as! SignUpViewController
        print(" first name \(parentVC.firstNameStr)")
//        print(" img str \(parentVC.imgStr)")
//        print(" selfie img str \(parentVC.selfieImgStr)")
    }
    
    func onRegistration() {
        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        self.view.addSubview(progressHUD)
        progressHUD.show()
        
        let parentVC = self.parent as! SignUpViewController
        //print(" first name \(parentVC.firstNameStr)")
        //print(" img str \(parentVC.imgStr)")
        
        let jsonObject1 = ["FirstName": parentVC.firstNameStr,
                           "MiddleName": parentVC.middleNameStr ,
                           "LastName": parentVC.lastNameStr ,
                           "Phone":  parentVC.phoneNumStr,
                           "CivilID": parentVC.civilIDNumStr ]
        
        let jsonObject2 = ["Email": parentVC.emailStr]
        
        let parameters = ["UserAccount": jsonObject1,
                          "Account": jsonObject2 ,
                          "Password": parentVC.passwordStr ,
                          "StrImage": parentVC.imgStr ,
                          "SelifImage": parentVC.selfieImgStr ,
                          "MobileID": "676767667"] as [String : Any]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: self)
        }
        
        AFManager.requestPOSTURL(URLs.URL_REGISTER, params: parameters as [String : AnyObject], success: { (response) in
            progressHUD.hide()
            //print(response)
            
            let alert = UIAlertController(title: "" , message: AFManager.changeMsg1(MsgCode: response["resCode"].intValue), preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler:{ (action) -> Void in
                
                self.dismiss(animated: true, completion: nil)
                
        }))
            self.present(alert, animated: true, completion: nil)
            
            
        }) { (error) in
            progressHUD.hide()
            //print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    

    @IBAction func BackBtnAction(_ sender: Any) {
        let parentVC = self.parent as! SignUpViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[3]], direction: .reverse, animated: true, completion: nil)
        SignUpViewController.pageControl.currentPage = 3
       // parentVC.configureTitleLbl(pageTitle: "second")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation
//
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
