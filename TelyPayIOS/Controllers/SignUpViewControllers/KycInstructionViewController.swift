//
//  KycInstructionViewController.swift
//  TelyPayIOS
//
//  Created by admin on 6/16/19.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class KycInstructionViewController: UIViewController {
    @IBOutlet weak var step1Lbl: UILabel!
    @IBOutlet weak var step2Lbl: UILabel!
    @IBOutlet weak var step1InstructionLbl: UILabel!
    @IBOutlet weak var step2InstructionLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var step1ImgBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollViewConstraintTop: NSLayoutConstraint!
    var isIPhoneX: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        step1Lbl.text = NSLocalizedString("step1", comment: "")
        step2Lbl.text = NSLocalizedString("step2", comment: "")
        step1InstructionLbl.text = NSLocalizedString("kyc_instruction_first_step_txt", comment: "")
        step2InstructionLbl.text = NSLocalizedString("kyc_instruction_second_step_txt", comment: "")
        
        
        let locale = NSLocale.current.languageCode
        //print(locale as Any)
        if locale == "ar" as String {
            backBtn.transform = backBtn.transform.rotated(by: .pi/1)
        }
        
        isIPhoneX = AppDelegate.init().isIPhoneXDevice()
        
        /// here adding the constraints based on the device
        if isIPhoneX
        {
            scrollViewConstraintTop.constant = 83
            step1ImgBottomConstraint.constant = 38

        }
        else
        {
            scrollViewConstraintTop.constant = 10
            step1ImgBottomConstraint.constant = 19
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func goBack(_ sender: Any) {
        let parentVC = self.parent as! SignUpViewController
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[0]], direction: .reverse, animated: true, completion: nil)
        SignUpViewController.pageControl.currentPage = 0
        
    }
    

    @IBAction func goCaptureID(_ sender: UIButton) {
        
        let parentVC = self.parent as! SignUpViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.pages[2]], direction: .reverse, animated: true, completion: nil)
        SignUpViewController.pageControl.currentPage = 2
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
