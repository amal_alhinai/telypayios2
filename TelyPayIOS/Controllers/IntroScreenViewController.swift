//
//  IntriScreenViewController.swift
//  TelyPayIOS
//
//  Created by admin on 9/30/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit

class IntroScreenViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var introScreenScrollview: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var slides:[Slide] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userDefaults = UserDefaultManager()
        if userDefaults.isAlreadyLoggedInBefore() {
            
            let loginPage = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? ViewController
            self.present(loginPage!, animated: true, completion: nil)
            
            self.dismiss(animated: true, completion: nil)
            
        }
        
        introScreenScrollview.delegate = self
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        pageControl.currentPageIndicatorTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5985788919)
        pageControl.pageIndicatorTintColor = #colorLiteral(red: 0.1006977562, green: 0.2920336034, blue: 0.5024970988, alpha: 0)
        view.bringSubviewToFront(pageControl)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        pageControl.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        
        //this loop for creating border for the dots of page control
        for index in 0..<3{ // your array.count
            let viewDot = pageControl.subviews[index]
            viewDot.layer.borderWidth = 0.5
            if (index == pageControl.currentPage){ // indexPath is the current indexPath of your selected cell or view in the collectionView i.e which needs to be highlighted
                viewDot.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.5985788919)
                viewDot.layer.borderColor = UIColor.white.cgColor
            }
            else{
                viewDot.backgroundColor = #colorLiteral(red: 0.1006977562, green: 0.2920336034, blue: 0.5024970988, alpha: 0)
                viewDot.layer.borderColor = UIColor.white.cgColor
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        
    }
    
    
    @objc func nextAction(sender:UIButton!) {
        print("Button Clicked")
        if(pageControl.currentPage == 0){
            self.introScreenScrollview.scrollTo(horizontalPage: 1)
        }else if(pageControl.currentPage == 1){
            self.introScreenScrollview.scrollTo(horizontalPage: 2)
        }else if(pageControl.currentPage == 2){
            skipAction(sender: sender)
        }
        
    }
    
    @objc func skipAction(sender:UIButton!) {
 
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "LoginViewController") as! ViewController
        self.present(newViewController, animated: true, completion: nil)
    }
    
    func createSlides() -> [Slide] {
        
        let slide1:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide1.slideImg.image = #imageLiteral(resourceName: "onboard_illustration_1")
        slide1.slideDescLbl.isEditable = false
        slide1.slideTitLbl.text = NSLocalizedString("intro_screen_1_tv_a", comment: "")
        slide1.slideDescLbl.text = NSLocalizedString("intro_screen_1_tv_b", comment: "")
        slide1.nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        slide1.skipBtn.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
        
      
        let slide2:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide2.slideImg.image = #imageLiteral(resourceName: "onboard_illustration_2")
        slide2.slideTitLbl.text = NSLocalizedString("intro_screen_2_tv_a", comment: "")
        slide2.slideDescLbl.isEditable = false
        slide2.slideDescLbl.text = NSLocalizedString("intro_screen_2_tv_b", comment: "")
        slide2.nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        slide2.skipBtn.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
        
        
        
        let slide3:Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide3.slideImg.image = #imageLiteral(resourceName: "onboard_illustration_3")
        slide3.slideTitLbl.text = NSLocalizedString("intro_screen_3_tv_a", comment: "")
        slide3.slideDescLbl.isEditable = false
        slide3.slideDescLbl.text = NSLocalizedString("intro_screen_3_tv_b", comment: "")
        slide3.slideDescLbl.hyperLink(originalText: slide3.slideDescLbl.text, hyperLink: NSLocalizedString("here", comment: ""), urlString: URLs.BASE_URL)
        slide3.nextBtn.addTarget(self, action: #selector(nextAction), for: .touchUpInside)
        slide3.skipBtn.addTarget(self, action: #selector(skipAction), for: .touchUpInside)
        
        
        return [slide1, slide2, slide3]
    }
    
    
    
    func setupSlideScrollView(slides : [Slide]) {
        introScreenScrollview.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        introScreenScrollview.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        introScreenScrollview.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            introScreenScrollview.addSubview(slides[i])
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UITextView {
    
    func hyperLink(originalText: String, hyperLink: String, urlString: String) {
        
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        
        let attributedOriginalText = NSMutableAttributedString(string: originalText)
        let linkRange = attributedOriginalText.mutableString.range(of: hyperLink)
        let fullRange = NSMakeRange(0, attributedOriginalText.length)
        attributedOriginalText.addAttribute(NSAttributedString.Key.link, value: urlString, range: linkRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.purple, range: fullRange)
        attributedOriginalText.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 10), range: fullRange)
        
        self.linkTextAttributes = convertToOptionalNSAttributedStringKeyDictionary([
            kCTForegroundColorAttributeName: UIColor.blue,
            kCTUnderlineStyleAttributeName: NSUnderlineStyle.single.rawValue,
            ] as [String : Any])
        
        
        self.attributedText = attributedOriginalText
        self.textAlignment = NSTextAlignment.center
        self.textColor =  UIColor (red: 17.0/255.0, green: 61.0/255.0, blue: 99/255.0, alpha: 0.5)
        self.font = UIFont.systemFont(ofSize: 18)
    }
    
}

extension UIScrollView {
    
    func scrollTo(horizontalPage: Int? = 0, verticalPage: Int? = 0, animated: Bool? = true) {
        var frame: CGRect = self.frame
        frame.origin.x = frame.size.width * CGFloat(horizontalPage ?? 0)
        frame.origin.y = frame.size.width * CGFloat(verticalPage ?? 0)
        self.scrollRectToVisible(frame, animated: animated ?? true)
    }
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
