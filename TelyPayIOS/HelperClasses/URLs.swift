//
//  URLs.swift
//  TelyPayIOS
//
//  Created by admin on 10/9/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation

class URLs: NSObject {
    
//    public static let BASE_URL: String = "https://telypay-web-app.azurewebsites.net"
    public static let BASE_URL: String = "https://telypay.com";

    
    public static let URL_REGISTER = BASE_URL + "/api/mobile/user";
    public static let URL_LOGIN = BASE_URL + "/token";
    public static let URL_GET_USER_DETAILS = BASE_URL + "/api/mobile/user";
    public static let URL_TOPUP = BASE_URL + "/api/mobile/topUp";
    public static let URL_GET_BALANCE = BASE_URL + "/api/mobile/balance";
    public static let URL_CHANGE_EMAIL = BASE_URL + "/api/mobile/email";
    public static let URL_CHANGE_PASSWORD = BASE_URL + "/api/mobile/password/";
    public static let URL_GET_DEVICE = BASE_URL + "/api/mobile/device";
    public static let URL_FORGOT_PASSWORD = BASE_URL + "/api/mobile/forget-password?Email=";
    public static let URL_CHECK_EMAIL_CIVILID_IF_EXIST = BASE_URL + "/api/mobile/user-availability?Email=";
    public static let URL_GET_ID_CARD = BASE_URL + "/api/mobile/idCard";
    public static let URL_UPDATE_ID_CARD = BASE_URL + "/api/mobile/idCard";
    public static let URL_UPDATE_SELFIE = BASE_URL + "/api/mobile/selfie";
    public static let URL_REMOVE_DEVICE = BASE_URL + "/api/mobile/device";
    public static let URL_TEMP_TOPUP = BASE_URL + "/api/mobile/TemtopUp";
    public static let URL_GET_TELECOMS = BASE_URL + "/api/mobile/telcom";
    public static let URL_LOGOUT = BASE_URL + "/api/mobile/LogOut";
    public static let URL_GET_SELFIE = BASE_URL + "/api/mobile/Selfie";
    public static let URL_GET_TOPUP_HISTORY = BASE_URL + "/api/mobile/CardHistory";
    
    //for tests
    public static let URL_RESET_DATA = BASE_URL + "/api/testUser/Email-verified";
    public static let URL_RESET_TOPUP_CARD = BASE_URL + "/api/testUser/Pins-verified";

}
