//
//  UserDefaultManager.swift
//  TelyPayIOS
//
//  Created by admin on 10/8/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation


class UserDefaultManager : NSObject{
    
    let prefs:UserDefaults = UserDefaults.standard

    
    func alreadyLoggedInBefore(x : Int) -> Void {
        prefs.set(x, forKey:"alreadyloggedinbefore")
    }
    
    func isAlreadyLoggedInBefore() -> Bool {
         return prefs.object(forKey: "alreadyloggedinbefore") as? Int == 1 ? true : false
    }


    
}
