//
//  KeyChainWrapperManager.swift
//  TelyPayIOS
//
//  Created by admin on 12/25/18.
//  Copyright © 2018 admin. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class KeyChainWrapperManager : NSObject{
  
    
    let keyChainPrefs: KeychainWrapper = KeychainWrapper.standard
    
    
    
    func setCurrentLoginUser(_ userDetails: User) {
    keyChainPrefs.set(userDetails.nameStr!, forKey:"username")
    keyChainPrefs.set(userDetails.emailStr!, forKey:"useremail")
    keyChainPrefs.set(userDetails.phoneStr!, forKey:"userphone")
    keyChainPrefs.set(userDetails.balance!, forKey:"userbalance")
    keyChainPrefs.set(userDetails.accessToken!, forKey:"accesstoken")
    keyChainPrefs.set(userDetails.accessTokenExpireIn!, forKey:"accesstokenexpirein")
    keyChainPrefs.set(userDetails.activationStatus!, forKey:"activationstatus")
    
    }
 
    func getLoggedUserDetails() -> User {
    let user = User()
    user.emailStr = keyChainPrefs.string(forKey: "useremail")
    user.nameStr  =  keyChainPrefs.string(forKey: "username")
    user.phoneStr = keyChainPrefs.string(forKey: "userphone")
    user.accessToken = keyChainPrefs.string(forKey: "accesstoken")
    user.accessTokenExpireIn = keyChainPrefs.object(forKey: "accesstokenexpirein") as? Int
    user.balance = keyChainPrefs.object(forKey: "userbalance") as? Float
    user.activationStatus = keyChainPrefs.object(forKey: "activationstatus") as? Int
    
    return user
    }
    
    func updateBlance(balance: Float) {
    keyChainPrefs.set(balance, forKey:"userbalance")
    }
    
    func isUserLoggedIN() -> Bool {
        return keyChainPrefs.accessibilityOfKey("accesstoken") != nil ? true : false
    }

    
    //this method to set the image
    func setIDCardUser(_ idCard: IDCard) {
        keyChainPrefs.set(idCard.imgStr!, forKey:"idcardimg")
        keyChainPrefs.set(idCard.selfieStr!, forKey:"selfieimg")
        keyChainPrefs.set(idCard.stateStr!, forKey:"stateidcardimg")
    }
    
    //this method to get the image
    func getIdCard() -> IDCard {
    let id_card = IDCard()
    id_card.imgStr = keyChainPrefs.string(forKey: "idcardimg")
    id_card.selfieStr = keyChainPrefs.string(forKey: "selfieimg")
    id_card.stateStr  =  keyChainPrefs.string(forKey: "stateidcardimg")
    
    return id_card
    }
    
    func logout() {
        keyChainPrefs.removeObject(forKey: "accesstoken")
        keyChainPrefs.removeObject(forKey: "useremail")
        keyChainPrefs.removeObject(forKey: "username")
        keyChainPrefs.removeObject(forKey: "userphone")
        keyChainPrefs.removeObject(forKey: "accesstokenexpirein")
        keyChainPrefs.removeObject(forKey: "userbalance")
        keyChainPrefs.removeObject(forKey: "activationstatus")
    }
    
    
 

    
    
    
    
    
}
