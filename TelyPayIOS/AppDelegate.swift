//
//  AppDelegate.swift
//  TelyPayIOS
//
//  Created by admin on 9/25/18.
//  Copyright © 2018 admin. All rights reserved.
//

import UIKit
import RSFloatInputView
import Firebase
import UserNotifications
import SwiftyJSON



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    var isIPhoneX: Bool = false
    let userDefault = UserDefaultManager()
    let keyChainWrapper = KeyChainWrapperManager()

//    func changeStatusBarBGColor() {
//        guard let statusBarView = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else {
//            return
//        }
//        statusBarView.backgroundColor = UIColor(red: 26/255.0, green: 74/255, blue: 128/255, alpha: 1.0)
//    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        var arguments = ProcessInfo.processInfo.arguments
        arguments.removeFirst()
        print("App launching with the following arguments: \(arguments)")
        
        // Always clear the defaults first
        if arguments.contains("ResetDefaults") {
            userDefault.alreadyLoggedInBefore(x: 0)
            keyChainWrapper.logout()
            resetAllAccountsDataAPIRequest()
            resetTopUpPinAPIRequest()
        }
        
        
       // changeStatusBarBGColor()
        FirebaseApp.configure()
        
        skippingIntroScreen()
        
        if (keyChainWrapper.isUserLoggedIN()){
            
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(withIdentifier: "profilePage")
            
            vc.modalTransitionStyle = .crossDissolve
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
        
        RSFloatInputView.stringTransformer = {
            orginal in
            // Transform the place holder string configured in XIB with your own way.
            // e.g return NSLocalizedString(orginal, comment: orginal)
            return orginal.replacingOccurrences(of: "TXT_", with: "")
        }
        RSFloatInputView.instanceTransformer = {
            instance in
            // Support multi-styles in one place using the tag
            if instance.tag == 0 {
                instance.floatPlaceHolderColor = UIColor.blue
                instance.textColor = UIColor.darkText
                instance.tintColor = UIColor.blue
            }
            if instance.tag == 1 {
                instance.floatPlaceHolderColor = UIColor.blue
                instance.textColor = UIColor.darkText
                instance.tintColor = UIColor.blue
            }
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        registerForPushNotifications(application)
        
        return true
    }
    
    //this method for calling a request to reset the test data
    func resetAllAccountsDataAPIRequest() {
        
                let headers = ["Accept":"application/json"]
                AFManager.requestTestPUTURL(URLs.URL_RESET_DATA, headers: headers, success: { (response) in
        
                    self.print(response)
        
                }) { (error) in
                    self.print(error)
                }
    }
    
    //this method for calling a request to reset the test data (card pins)
    func resetTopUpPinAPIRequest() {
        
        let headers = ["Accept":"application/json"]
        AFManager.requestTestPUTURL(URLs.URL_RESET_TOPUP_CARD, headers: headers, success: { (response) in
            
            self.print(response)
            
        }) { (error) in
            self.print(error)
        }
    }

    
    //this method for skip intro screen if user already logged in before
    func skippingIntroScreen() {
        if (userDefault.isAlreadyLoggedInBefore()){
            
            let sb = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc = sb.instantiateViewController(withIdentifier: "LoginViewController")
            
            vc.modalTransitionStyle = .crossDissolve
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
    }
    
    func logout() {
        
        logoutReq()
        
    }
    
    func logoutWhenSessionExpire() {
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let loginPageVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! ViewController
        
        keyChainWrapper.logout()
        loginPageVC.modalTransitionStyle = .crossDissolve
        appDel.window!.rootViewController = loginPageVC
        appDel.window!.makeKeyAndVisible()
        alertUser(strTitle: "", strMessage: NSLocalizedString("session_expired", comment: ""), viewController: loginPageVC)
        
    }
    
    
    func logoutReq() {
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let loginPageVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! ViewController

        
        let progressHUD = ProgressHUD(text: NSLocalizedString("please_wait", comment: ""))
        loginPageVC.view.addSubview(progressHUD)
        progressHUD.show()
        
        let headers = ["Authorization":"bearer " + keyChainWrapper.getLoggedUserDetails().accessToken!,
                       "Content-Type":"application/json; charset=utf-8"]
        
        if !AFManager.Connectivity.isConnectedToInternet {
            print("no internet is available.")
            AppDelegate.init().noInternetConnectionMsg(viewController: loginPageVC)
        }
        
        AFManager.strRequestPOSTURL2(URLs.URL_LOGOUT, headers: headers, success: { (response) in
            progressHUD.hide()
            self.print(response)
            
            self.keyChainWrapper.logout()
            loginPageVC.modalTransitionStyle = .crossDissolve
            appDel.window!.rootViewController = loginPageVC
            appDel.window!.makeKeyAndVisible()
            
            
        }) { (error) in
            progressHUD.hide()
            self.print(error)
            let alert = UIAlertController(title: "", message: NSLocalizedString("some_error_occured", comment: "") , preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: UIAlertAction.Style.default, handler: nil))
            loginPageVC.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    public func alertUser(strTitle: String, strMessage: String, viewController: UIViewController) {
        let myAlert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        myAlert.addAction(okAction)
        viewController.present(myAlert, animated: true, completion: nil)
    }
    
    public func noInternetConnectionMsg(viewController: UIViewController){
        
        alertUser(strTitle: "", strMessage: NSLocalizedString("no_internen_connection_msg", comment: ""), viewController: viewController)
    }
    
    func isIPhoneXDevice() -> Bool {
        
        /// this "if statement" for checking if the device the app running in is iphonex or not,
        ///we need this becuase iphonx screen size is differnt from other devices so we should have new constraints so its look good on it
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                isIPhoneX = false
            case 1334:
                print("iPhone 6/6S/7/8")
                isIPhoneX = false
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                isIPhoneX = false
            case 2436:
                print("iPhone X, Xs")
                isIPhoneX = true
            case 2688:
                print("iPhone Xs Max")
                isIPhoneX = true
            case 1792:
                print("iPhone Xr")
                isIPhoneX = true
            default:
                print("unknown")
                isIPhoneX = false
            }
        }
        
        return isIPhoneX
    }
    
    //this method for detecting if the device the app running in is jail broken
    func isJailbroken() -> Bool {
        #if arch(i386) || arch(x86_64)
        // This is a Simulator not an idevice
        return false
        #else
        let fm = FileManager.default
        if(fm.fileExists(atPath: "/private/var/lib/apt") ||
            fm.fileExists(atPath: "/Applications/Cydia.app") ||
            fm.fileExists(atPath: "/bin/bash") ||
            fm.fileExists(atPath: "/usr/sbin/sshd") ||
            fm.fileExists(atPath: "/etc/apt") ||
            fm.fileExists(atPath: "/Library/MobileSubstrate/MobileSubstrate.dylib")
            ) {
            // This Device is jailbroken
            return true
        } else {
            // Continue the device is not jailbroken
            return false
        }
        
        #endif
        
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("User Info = \(notification.request.content.body)")
        completionHandler([.alert, .badge, .sound])
        
    }
    // Called to let your app know which action was selected by the user for a given notification.
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("User Info = \(response.notification.request.content.body)")
        completionHandler()
    }
    
    
    func registerForPushNotifications(_ application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
//                    UIApplication.shared.registerForRemoteNotifications()
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                else{
                    //Do stuff if unsuccessful…
                }
            })
        } else {
            let settings = UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        
        UserDefaults.standard.setValue(token, forKey: "kDeviceToken")
        UserDefaults.standard.synchronize()
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError
        error: Error) {
        // Try again later.
//        print(" rrrrrrrr \(error)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        //if it is jail broken then show alert and close the app
        if isJailbroken() {
            let alert = UIAlertController(title: NSLocalizedString("jailbreak_check_dialog_title", comment: ""), message: NSLocalizedString("jailbreak_check_dialog_msg", comment: ""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .cancel, handler: { (action: UIAlertAction!) in
             UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
            
        }))
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // this function for removing the prints from all pages
    func print(_ items: Any...) {
        #if DEBUG
        Swift.print(items[0])
        #endif
    }

}


